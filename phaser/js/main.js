/**
 * Phaser must be configured much like our phones have settings or configuration OR like a computer game has settings/config
 * Below, 'config' is a variable that points to or refers to an object that holds all the configuration information.
 * The object starts with '{' and ends with '}'
 */
let config = {
    type: Phaser.AUTO,
    /* specifies the 'type' of rendering engine / how graphics will be drawn */
    width: 800,
    /* specifies the width of the game in pixels */
    height: 600,
    /* specifies the height of the game in pixels */
    scene: {
        /* 'scene' is an object within an object */
        preload: preload,
        /* preload is a function that runs ONCE only and is used to load game assets */
        create: create,
        /* create is a function that  ONCE only and is used to create game objects/things/characters */
        update: update /* update is a function that runs over and over again and is used to update positions of game characters */
    }
};

/* Create a new Phaser object */
let game = new Phaser.Game(config);

/**
 * The preload() function is where all game assets are loaded into Chrome's memory so that they can be
 * accessed very quickly by the game code. It is a good way of organising our game.
 */
function preload() {
    /* Load assets from the assets/images directory to make loading images easier - see below */
    this.load.setBaseURL('../../phaser/assets/images/');

    /* Load the player as a 'sprite'. A sprite is just an image capable of animation. In our case,
    we are using a simple square but sprites can be much more complicated.*/

    this.load.spritesheet('player',
        '32x32.png', {
            frameWidth: 32,
            frameHeight: 32
        }
    );
}

/**
 * The create function is used to put images on-screen initially, create game objects and generally do
 * once off initialisation. Once 'create' has run, we should be able to see our game in the browser
 */
function create() {
    /* Set up cursor keys */
    game.cursors = this.input.keyboard.createCursorKeys();

    /* All we are doing in create, is plonking our player on the screen at {x:400, y:300} */
    game.player = this.add.sprite(400, 300, 'player');


}

function update() {
    /* Check if left or down key has been pressed */
    if (game.cursors.left.isDown) {
        game.player.x -= 1;
    } else if (game.cursors.down.isDown) {
        game.player.y += 1;
    }

    /**
     * CHALLENGE:
     * 
     * Can you work out what code to add to check for the up and down keys as well ?
     */
}